package Codility.Lesson1;

public class BinaryGap {
	
	public static void main(String[] args) {
		
		int ans = solution(10);
		System.out.println("ans = " + ans);
	}
	
	
	/*
    public static int solution(int N) {
        return binaryGap(N,0,0,0);
    }
    
    public static int binaryGap(int n, int counter, int max, int index){
        
        if(n==0)
            return max;

        if(n%2==0 && index==0){
            index=0;
        }else if(n%2==0){
            counter++;
        }else{
            max = Math.max(counter, max);
            index++;
            counter=0;
        }
        n = n/2;
        return binaryGap(n, counter, max, index);
    } //end of public int binaryGap(int n, int counter, int max, int index){
    */
    
    static int solution(int n) {
        return solution(n, 0, 0, 0);
    }
	
    static int solution(int n, int max, int current, int index) {
        if (n == 0)
            return max;
        else if (n % 2 == 0 && index == 0)
            return solution(n / 2, max, current, 0);
        else if (n % 2 == 0 && index > 0)
            return solution(n / 2, max, current + 1, index + 1);
        else
            return solution(n / 2, Math.max(max, current), 0, index + 1);
    }

} //end of public class BinaryGap {


// https://stackoverflow.com/questions/35531747/solving-binary-gap-using-recursion


