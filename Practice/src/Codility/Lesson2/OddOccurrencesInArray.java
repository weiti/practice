package Codility.Lesson2;

public class OddOccurrencesInArray {
	
	public static int solution(int[] A) {
		
		int ele = 0;		
		for(int i=0; i<A.length; i++){
			ele ^= A[i];						
		} //end of for()		
		return ele;
	} //end of public static int solution(int[] A) {
	
}

// https://github.com/karimhamdanali/codility/blob/master/src/codility/OddOccurrencesInArray.java
// https://codility.com/demo/results/trainingAXBZG9-NUW/
