package Codility.Lesson2;

public class CyclicRotation {
	
	public int[] solution(int[] A, int K) {
        // write your code in Java SE 8
		
		if(A.length == 0 || K == 0){
			return A;
		}
		
		int shiftNum = K%A.length;		
		int[] rotA = new int[A.length];
		
		for(int i=0; i<A.length; i++){
			
			int idx;
			if(i + shiftNum >= A.length){
				idx = i + shiftNum - A.length;		
			}else{
				idx = i + shiftNum;
			}
			rotA[idx] = A[i];			
		} //end of for(int i=0; i<A.length; i++){
		
		return rotA;
    } //end of public int[] solution(int[] A, int K) {
}

// https://codility.com/demo/results/trainingQS7DUA-J42/
// https://github.com/styro103/CyclicRotation/blob/master/CyclicRotation.java
