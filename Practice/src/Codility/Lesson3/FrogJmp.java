package Codility.Lesson3;

public class FrogJmp {
	
	public static void main(String[] args) {
		
		int jumpCnt = solution(30, 85, 30);
		System.out.println("jumpCnt = " + jumpCnt);
	}
	
    public static int solution(int X, int Y, int D) {
        
    	double distance = Y - X;
    	int jumpCnt = (int)Math.ceil(distance/D);
    	return jumpCnt;
    }
	
}
