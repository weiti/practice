package Codility.Lesson3;

public class TapeEquilibrium {
	
	public int solution(int[] A) {

		int sumPre = A[0];
	    int sumPost = 0;
	    
	    for (int i = 1; i < A.length; i++) {
	        sumPost += A[i];
	    }
	    
	    int difMin = Math.abs(sumPost - sumPre);
	    int tempSub = 0;
	    
	    for (int i = 1; i < A.length - 1; i++) {
	    
	    	sumPre += A[i];
	        sumPost -= A[i];
	        tempSub = Math.abs(sumPost - sumPre);

	        if (tempSub < difMin) {
	            difMin = tempSub;
	        }
	    }
	    return difMin;
	} //end of public int solution(int[] A) {
} //end of public class TapeEquilibrium {

// https://codility.com/demo/results/training4DAF7U-4E5/



