package Codility.Lesson3;

public class PermMissingElem {

	public static void main(String[] args) {
		
		int[] A = {2,3,1,5};
		int missInt = solution(A);
		System.out.println("missInt = " + missInt);		
	}
	
    public static int solution(int[] A) {

    	long N = A.length + 1;
        long total = N * (N + 1) / 2;

        for (int i : A) {
            total -= i;
        }
        return (int)total;
    } //end of public static int solution(int[] A) {	
	
}
